<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace cfd\doc;

use yii\web\AssetBundle;

class DocAsset extends AssetBundle
{
    public $sourcePath = '@vendor/jiang704593835/yii2-doc/assets';
    public $css = [
        'container.min.css',
        'label.min.css',
        'message.min.css',
        'semantic.min.css',
        'table.min.css',
    ];
}
