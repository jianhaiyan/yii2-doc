<?php
namespace cfd\doc\components;
use phpDocumentor\Reflection\DocBlock\Tags\Method;
use Yii;
use yii\base\BaseObject;
use yii\caching\TagDependency;
use yii\helpers\VarDumper;

class Route extends BaseObject
{
    const CACHE_TAG = 'cfd.doc.routes';
    public $cacheDuration = 3600;
    public $isCache = false;

    public $model_data = [];
    public $model_name = '';

    /**
     * Get avaliable and assigned routes
     * @return array
     */
    public function getRoutes()
    {
        return $this->getAppRoutes();

    }

    /**
     * Get list of application routes
     * @return array
     */
    public function getAppRoutes($module = null)
    {
        if ($module === null) {
            $module = Yii::$app;
        } elseif (is_string($module)) {
            $module = Yii::$app->getModule($module);
        }

        $key = [__METHOD__, $module->getUniqueId()];
        $cache = Yii::$app->cache;
        if (true || $cache === null || !$this->isCache || ($result = $cache->get($key)) === false) {
            $result = [];

            $this->getRouteRecrusive($module, $result);
            if ($cache !== null && $this->isCache) {
                $cache->set($key, $result, $this->cacheDuration, new TagDependency([
                    'tags' => self::CACHE_TAG,
                ]));
            }
        }

        return $result;
    }

    /**
     * Get route(s) recrusive
     * @param \yii\base\Module $module
     * @param array $result
     */
    protected function getRouteRecrusive($module, &$result)
    {
        $token = "Get Route of '" . get_class($module) . "' with id '" . $module->uniqueId . "'";

        Yii::beginProfile($token, __METHOD__);
        try {
            foreach ($module->getModules() as $id => $child) {
                if(in_array($id,['debug','gii'])) continue;
                if (($child = $module->getModule($id)) !== null) {
                    $this->getRouteRecrusive($child, $result);
                }
            }
            foreach ($module->controllerMap as $id => $type) {
                $this->getControllerActions($type, $id, $module, $result);
            }

            $namespace = trim($module->controllerNamespace, '\\') . '\\';
            $this->getControllerFiles($module, $namespace, '', $result);
            $all = '/' . ltrim($module->uniqueId . '/*', '/');
            $result[$all] = $all;
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }

        Yii::endProfile($token, __METHOD__);

    }

    /**
     * Get list controller under module
     * @param \yii\base\Module $module
     * @param string $namespace
     * @param string $prefix
     * @param mixed $result
     * @return mixed
     */
    protected function getControllerFiles($module, $namespace, $prefix, &$result)
    {
        $path = Yii::getAlias('@' . str_replace('\\', '/', $namespace), false);
        $token = "Get controllers from '$path'";
        Yii::beginProfile($token, __METHOD__);
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file) && preg_match('%^[a-z0-9_/]+$%i', $file . '/')) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $baseName = substr(basename($file), 0, -14);
                    ##@todo 更新正则表达式
                    // $name = strtolower(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $baseName));
                    // var_dump($id = ltrim(str_replace(' ', '-', $name), '-'));
                    $name = strtolower(preg_replace('/([A-Z])/', ' \0', $baseName));
                    $id   = str_replace(' ', '-', ltrim($name));
                    $className = $namespace . $baseName . 'Controller';
                    if(in_array($baseName,['Doc'])) continue;
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * Get list action of controller
     * @param mixed $type
     * @param string $id
     * @param \yii\base\Module $module
     * @param string $result
     */
    protected function getControllerActions($type, $id, $module, &$result)
    {
        $token = "Create controller with cofig=" . VarDumper::dumpAsString($type) . " and id='$id'";
        Yii::beginProfile($token, __METHOD__);
        try {
            if($module->getUniqueId()=='doc' && $id!='demo'){
                return ;
            }
            /* @var $controller \yii\base\Controller */
            $controller = Yii::createObject($type, [$id, $module]);
            $this->getActionRoutes($controller, $result);
            $all = "/{$controller->uniqueId}/*";
            $class = new \ReflectionClass($controller);
            $docComment = $class->getDocComment();
            $docCommentArr = explode("\n", $docComment);
            $result[$all] = ['name'=>$all];
            foreach ($docCommentArr as $comment) {
                $comment = trim($comment);
                //@desc注释
                $pos = stripos($comment, '@doc-module');
                if ($pos !== false) {
                    $result[$all]['name'] = substr($comment, $pos + 11);
                    continue;
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * Get route of action
     * @param \yii\base\Controller $controller
     * @param array $result all controller action.
     */
    protected function getActionRoutes($controller, &$result)
    {
        $description = '';
        $descComment = '//请使用@desc 注释';
        $typeMaps = array(
            'string' => '字符串',
            'int' => '整型',
            'float' => '浮点型',
            'boolean' => '布尔型',
            'date' => '日期',
            'array' => '数组',
            'fixed' => '固定值',
            'enum' => '枚举类型',
            'object' => '对象',
        );
        $token = "Get actions of controller '" . $controller->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            $prefix = '/' . $controller->uniqueId . '/';
            foreach ($controller->actions() as $name => $value) {
                $comment_method  = $name.'_comment';
                if(method_exists($controller,$comment_method)){
                    $method = new \ReflectionMethod($controller,$comment_method);
                    $docComment = $method->getDocComment();
                    $id = $prefix . ltrim(str_replace(' ', '-', $name), '-');

                    if(!empty($method->getParameters())){
                        //特殊处理带ID的方法
                        $id.='/:id';
                    }
                    //$result[$id] = $id;
                    $result[$id] = [
                        'id' => $id,
                        'description' => '',
                        'descComment' => '//请使用@desc 注释',
                        'request' => [],
                        'response' => [],
                    ];

                    $this->format_comment($docComment,$result,$id,$method);
                }
            }

            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $key11 => $method) {
                $name = $method->getName();
                if ($method->isPublic() && !$method->isStatic() && strpos($name, 'action') === 0 && $name !== 'actions') {
                    ##@todo 更新正则表达式
                    // $name = strtolower(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', substr($name, 6)));
                    $name = strtolower(preg_replace('/[A-Z]/', ' \0', substr($name, 6)));

                    $id = $prefix . ltrim(str_replace(' ', '-', $name), '-');
                    if(!empty($method->getParameters())){
                        //特殊处理带ID的方法
                        $id.='/:id';
                    }
                    $docComment = $method->getDocComment();
                    if($this->format_comment($docComment,$result,$id,$method)){

                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * @param  string $docComment
     * @param array $result
     * @param  string $id
     * @param \ReflectionMethod $method
     */
    private function format_comment($docComment,&$result,$id,$method){
        preg_match_all("/doc-(.*)/",$docComment,$matches);
        if(isset($matches[1]) && !empty($matches[1])){
            if(!isset($result[$id])){
                $result[$id] = [
                    'id' => $id,
                    'description' => '',
                    'descComment' => '//请使用@desc 注释',
                    'request' => [],
                    'response' => [],
                ];
            }
            foreach($matches[1] as $line){
                $arr = explode(" ",trim($line));
                switch ($arr[0]){
                    case 'name':
                        $result[$id]['description'] = $arr[1];
                        break;
                    case 'desc':
                        $result[$id]['descComment'] = $arr[1];
                        break;
                    case 'param':
                        $result[$id]['request'][] = [
                            'name' => $arr[2],
                            'type' => $arr[1],
                            'require' => isset($arr[5]) && $arr[5]=='optional'? false:true,
                            'default' => isset($arr[4])?$arr[4]:'',
                            'other' => '',
                            'desc' => $arr[3]??''
                        ];
                        break;
                    case 'version':
                        $result[$id]['version'] = $arr[1];
                        break;
                    case 'author':
                        $result[$id]['author'] = $arr[1];
                        break;
                    case 'return':
                        array_shift($arr);
                        $result[$id]['response'][] = $arr;
                        break;
                    case 'define':

                        if(isset($arr[2])){
                            $earr = explode('.',$arr[2]);
                            if(!isset($earr[1])){
                                $result['objectlist'][$earr[0]] = [];
                            }else{
                                $result['objectlist'][$earr[0]][] = [
                                    $arr[1],
                                    $earr[1],
                                    $arr[3]
                                ];
                            }
//                            ['object',$object_name,$returnCommentArr[1]];
                        }

                        break;
                }
            }
            return true;
        }
        return false;
    }

    private function set_model_name($name){

        $this->model_name = $name;

        $className = '\common\models\\'.$this->model_name;

        if(class_exists($className)){
            $model = new $className();
        }else{
            $this->model_data[] = '';
            return false;
        }

        $attributes = $model->attributes;

        $attributes_key = array_keys($attributes);

        foreach ($attributes_key as $key => $row) {

            $db_type = $model::getTableSchema($this->model_name)->columns[$row]->dbType;
            $db_type_common = (isset($model->attributeLabels()[$row]))?$model->attributeLabels()[$row]:$model::getTableSchema()->columns[$row]->comment;
            $this->model_data[$row] = [$db_type,$row,$db_type_common];
        }

    }

    private function set_model_data($attribute="",$type='extra'){

        $className = '\common\models\\'.$this->model_name;

        if(class_exists($className)){
            $model = new $className();
        }else{
            $this->model_data[] = '';
            return false;
        }

        $attributes = $model->attributes;

        $attributes_key = array_keys($attributes);

        switch ((string)$type) {
            case 'need':
                //处理需要的属性 eg：id,name
                $attribute_need = explode(',', $attribute);

                foreach ($attributes_key as $key => $row) {

                    if(!in_array($row, $attribute_need)){

                        unset($this->model_data[$row]);

                    }
                }

                break;
            case 'needless':
                //处理不需要的属性eg：mobile,zone
                $attribute_needless = explode(',', $attribute);

                foreach ($attribute_needless as $key => $row) {

                    if(isset($this->model_data[$row])){

                        unset($this->model_data[$row]);

                    }
                }

                break;
            case 'extra':
                //处理额外需要的属性eg：['int','abc','额外的参数'],['string','ac','外的参数']

                if(preg_match_all("/(\[.*?\])/", $attribute,$match)){

                    if($match){
                        foreach ($match[1] as $key => $value) {
                            $row = str_replace("'", '', $value);
                            $row = preg_replace("/[\[\]]/", '', $row);
                            // $length = mb_strlen($row);
                            // $row = mb_substr($row, 1,$length - 2);
                            $extra_array = explode(',', $row);
                            $this->model_data[$extra_array[1]] = $extra_array;
                        }
                    }
                }

                break;
            default:
                $this->model_data = [];
                break;
        }
    }

    private function get_model_data(){
        return $this->model_data;
    }
}
