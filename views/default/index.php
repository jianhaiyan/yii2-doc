<?php
/**
 * @var \cfd\doc\components\DocObject $docObject
 */
/* @var $this \yii\web\View */
/* @var $content string */
/**
 * @param \cfd\doc\components\DocObject $docObject
 * @param $param
 * @param int $default_px
 */
function humpToUnderLine($str)
{
    $len = strlen($str);
    $out = strtolower($str[0]);
    for ($i = 1; $i < $len; $i++) {
        if (ord($str[$i]) >= ord('A') && (ord($str[$i]) <= ord('Z'))) {
            $out .= '_' . strtolower($str[$i]);
        } else {
            $out .= $str[$i];
        }
    }
    return $out;
}
function render_object($docObject,$param,$default_px=64){
    $str = '';
    if(preg_match('/(.*?)(?:\{)(.*)(?:\})/i', $param[1], $match) && isset($match[1]) && isset($match[2])):
        $pleft = $default_px;
        if(strpos($match[1],'@')!==false):
            $name = explode('-',$match[1]);
            $str .= '<tr>';
            $str .= '<td style="padding-left: '.$pleft.'px;">'.lcfirst(substr($name[0],1)).'</td>';
            $str .= '<td>'.htmlspecialchars($param[0]) .'</td>';
            $str .= '<td>'.$param[2] .'</td>';
            $str .='</tr>';
            $pleft = $pleft + 32;
        endif;
        foreach ($docObject->getObjectNew($param[1]) as $p3):
            $return_str = render_object($docObject,$p3,$pleft);
            $str  .= $return_str;
        endforeach;
    else:
        $str .= '<tr>';
        $str .= '<td style="padding-left: '.$default_px.'px;">'.lcfirst($param[1]).'</td>';
        $str .= '<td>'.htmlspecialchars($param[0]) .'</td>';
        $str .= '<td>'.$param[2] .'</td>';
        $str .='</tr>';
    endif;
    return $str;
}
\cfd\doc\DocAsset::register($this);
?>
<div>
    <?php
    $domain = "http://".Yii::$app->request->serverName;
    if(Yii::$app->request->serverPort!=80){
        $domain .=':'.Yii::$app->request->serverPort;
    }
    ?>

    <style>
        .top-nav{width: 1024px;margin:0 auto;text-align:center;}
        .top-nav a{width:128px;text-align:center;display: inline-block;font-size:16px;color:#000;padding:10px 14px;background: #00ACED;}
        .top-nav a:hover{text-decoration:underline; background: #1e70bf;}
        .top-nav .active{text-decoration:underline; background: #1e70bf;}
        .top-nav .left-a{}
        .top-nav .right-a{}
    </style>
    <div class="top-nav">
        <?php foreach ($module_map as $m_k=>$m_name):?>
            <a href="<?php echo $domain;?>/doc?m=<?=$m_k;?>" class="left-a <?php if($m==$m_k): ?>active<?php endif;?>"><?=$m_name;?></a>
        <?php endforeach;?>
    </div>

    <h3 style="margin-left: 32px;">测试域名：<?php echo $domain;?><br/>Postman JSON：<?php echo $domain;?>/doc/default/postman-json<?php if($m=='mv1'){echo '?m=mv1';}elseif($m=='v2'){echo '?m=v2';}elseif($m=='mv2'){echo '?m=mv2';} ?></h3>
    <!--
    <pre>
        http 请求头参数<br/>
            Authorization  Bearer {{token}}<br/>
            device-id 值  设备ID<br/>
            version   值  客户端版本号<br/>
            lat       值  用户坐标纬度<br/>
            lng       值  用户坐标经度<br/>
    </pre>
    -->
    <pre>
        全局参数(header)
        Authorization  Bearer {{token}}
    </pre>
</div>
<div class="ui r" style="max-width: none !important;">
    <div class="site-tree">
        <ul class="layui-tree">
            <?php foreach ($routes as $item): ?>
                <?php if(empty($item['children'])) continue;?>

                <li><h2><a href="#<?= $item['id'] ?>"><?= $item['name'] ?></a></h2></li>

                <?php foreach($item['children'] as $i2):?>

                    <li class="site-tree-noicon <?= $i2['id'] == '' ? 'layui-this' : '' ?>">
                        <a href="#<?= $i2['id'] ?>"><cite><?= $i2['description'] ?></cite></a>
                    </li>
                <?php endforeach;?>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="site-content">
        <?php foreach ($routes as $mitem): ?>
            <?php if(empty($mitem['children'])) continue;?>
            <div id="<?= $mitem['id'] ?>">
                <h1 class="site-h1"><?= $mitem['name'] ?></h1>
            </div>
            <?php foreach($mitem['children'] as $item):?>
                <div id="<?= $item['id'] ?>">
                    <div class="ui raised segment">
                        <span class="ui red ribbon label"><?= $item['description'] ?></span>
                        <div class="ui message">
                            <p>接口链接：<?= $item['id'] ?></p>
                            <p>接口类型：POST</p>
                            <p>说明：<?= $item['descComment'] ?></p>
                            <p>联系人：<?=isset($item['author'])?$item['author']:'';?></p>
                        </div>
                        <table class="ui red celled striped table">
                            <thead>
                            <tr>
                                <th>请求参数</th>
                                <th>类型</th>
                                <th>是否必须</th>
                                <th>默认值</th>
                                <th>其他</th>
                                <th>说明</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($item['request'] as $param): ?>
                                <tr>
                                    <td><?= $param['name'] ?></td>
                                    <td><?= $param['type'] ?></td>
                                    <td><?= $param['require'] ? '是' : '否' ?></td>
                                    <td><?= $param['default'] ?></td>
                                    <td><?= $param['other'] ?></td>
                                    <td><?= $param['desc'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <table class="ui blue celled striped table">
                            <thead>
                            <tr>
                                <th>返回参数</th>
                                <th>类型</th>
                                <th>说明</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>errno</td>
                                <td>number</td>
                                <td>错误码</td>
                            </tr>

                            <tr>
                                <td>msg</td>
                                <td>string</td>
                                <td>错误信息</td>
                            </tr>

                            <tr>
                                <td>data</td>
                                <td>mixed</td>
                                <td>数据</td>
                            </tr>

                            <?php foreach ($item['response'] as $param): ?>
                                <?php if(preg_match('/(.*?)(?:\{)(.*)(?:\})/i', $param[1], $match) && isset($match[1]) && isset($match[2])): ?>

                                    <?php $pleft = 32;?>
                                    <?php if(strpos($match[1],'@')!==false):?>
                                        <tr>
                                            <td style="padding-left: <?= $pleft?>px;"><?= lcfirst(substr($match[1],1)) ?></td>
                                            <td class="td_type"><?= htmlspecialchars($param[0]) ?></td>
                                            <td><?= $param[2] ?></td>
                                        </tr>
                                        <?php $pleft = $pleft + 32;?>
                                    <?php endif;?>

                                    <?php foreach ($docObject->getObjectNew($param[1]) as $p2): ?>
                                        <?php echo render_object($docObject,$p2,$pleft);?>
                                    <?php endforeach;?>

                                <?php else:?>
                                    <tr>
                                        <td style="padding-left: 32px;"><?= lcfirst($param[1]) ?></td>
                                        <td class="td_type"><?= htmlspecialchars($param[0]) ?></td>
                                        <td><?= $param[2] ?></td>
                                    </tr>
                                <?php endif;?>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <br/>
                </div>
            <?php endforeach;?>
        <?php endforeach; ?>
    </div>

    <div class="layui-footer footer footer-doc">
        <div class="layui-main">
            <p>
            </p>
        </div>
    </div>
</div>